package com.petertran.trackify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petertran.trackify.model.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

}
