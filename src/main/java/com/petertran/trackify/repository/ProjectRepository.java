package com.petertran.trackify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petertran.trackify.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
