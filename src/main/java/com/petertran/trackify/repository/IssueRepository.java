package com.petertran.trackify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petertran.trackify.model.Issue;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Long> {

}
