package com.petertran.trackify.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petertran.trackify.exception.ResourceNotFoundException;
import com.petertran.trackify.model.Comment;
import com.petertran.trackify.repository.CommentRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/comments")
public class CommentController {
	
	private final CommentRepository commentRepository;

	@Autowired
	public CommentController(CommentRepository commentRepository) {
		this.commentRepository = commentRepository;
	}

	@GetMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<Comment> getAllComments() {
		return commentRepository.findAll();
	}
	
	@GetMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Comment> getCommentById(@PathVariable("id") Long commentId)
		throws ResourceNotFoundException {
			Comment comment = commentRepository.findById(commentId)
					.orElseThrow(() -> new ResourceNotFoundException("Id " + commentId + " not found"));
			return ResponseEntity.ok().body(comment);
	}
	
	@PostMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Comment createComment(@Valid @RequestBody Comment comment) {
		Date creationDate = new Date();
		comment.setCreateDate(creationDate);
		comment.setUpdateDate(creationDate);
		return commentRepository.save(comment);
	}

	@PutMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Comment> updateComment(@PathVariable("id") Long commentId,
			@Valid @RequestBody Comment commentDetails) throws ResourceNotFoundException {
		Comment comment = commentRepository.findById(commentId)
				.orElseThrow(() -> new ResourceNotFoundException("Id " + commentId + " not found"));

		comment.setContent(commentDetails.getContent());
		comment.setUpdateDate(new Date());
		final Comment updatedComment = commentRepository.save(comment);
		return ResponseEntity.ok(updatedComment);
	}

	@DeleteMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Map<String, Boolean> deleteComment(@PathVariable("id") Long commentId)
			throws ResourceNotFoundException {
		Comment comment = commentRepository.findById(commentId)
				.orElseThrow(() -> new ResourceNotFoundException("Id " + commentId + " not found"));

		commentRepository.delete(comment);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
