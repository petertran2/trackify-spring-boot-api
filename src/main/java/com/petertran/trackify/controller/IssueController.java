package com.petertran.trackify.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petertran.trackify.exception.ResourceNotFoundException;
import com.petertran.trackify.model.Issue;
import com.petertran.trackify.repository.IssueRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/issues")
public class IssueController {
	
	private final IssueRepository issueRepository;

	@Autowired
	public IssueController(IssueRepository issueRepository) {
		this.issueRepository = issueRepository;
	}

	@GetMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<Issue> getAllIssues() {
		return issueRepository.findAll();
	}
	
	@GetMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Issue> getIssueById(@PathVariable("id") Long issueId)
		throws ResourceNotFoundException {
			Issue issue = issueRepository.findById(issueId)
					.orElseThrow(() -> new ResourceNotFoundException("Id " + issueId + " not found"));
			return ResponseEntity.ok().body(issue);
	}
	
	@PostMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Issue createIssue(@Valid @RequestBody Issue issue) {
		Date creationDate = new Date();
		issue.setCreateDate(creationDate);
		issue.setUpdateDate(creationDate);
		return issueRepository.save(issue);
	}

	@PutMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Issue> updateIssue(@PathVariable("id") Long issueId,
			@Valid @RequestBody Issue issueDetails) throws ResourceNotFoundException {
		Issue issue = issueRepository.findById(issueId)
				.orElseThrow(() -> new ResourceNotFoundException("Id " + issueId + " not found"));

		issue.setPriority(issueDetails.getPriority());
		issue.setSubject(issueDetails.getSubject());
		issue.setStatus(issueDetails.getStatus());
		issue.setDescription(issueDetails.getDescription());
		issue.setDueDate(issueDetails.getDueDate());
		issue.setAssigneeId(issueDetails.getAssigneeId());
		issue.setUpdateDate(new Date());
		final Issue updatedIssue = issueRepository.save(issue);
		return ResponseEntity.ok(updatedIssue);
	}

	@DeleteMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Map<String, Boolean> deleteIssue(@PathVariable("id") Long issueId)
			throws ResourceNotFoundException {
		Issue issue = issueRepository.findById(issueId)
				.orElseThrow(() -> new ResourceNotFoundException("Id " + issueId + " not found"));

		issueRepository.delete(issue);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
