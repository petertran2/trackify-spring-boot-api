package com.petertran.trackify.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.petertran.trackify.exception.ResourceNotFoundException;
import com.petertran.trackify.model.Project;
import com.petertran.trackify.repository.ProjectRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/v1/projects")
public class ProjectController {
	
	private final ProjectRepository projectRepository;

	@Autowired
	public ProjectController(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}

	@GetMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<Project> getAllProjects() {
		return projectRepository.findAll();
	}
	
	@GetMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Project> getProjectById(@PathVariable("id") Long projectId)
		throws ResourceNotFoundException {
			Project project = projectRepository.findById(projectId)
					.orElseThrow(() -> new ResourceNotFoundException("Id " + projectId + " not found"));
			return ResponseEntity.ok().body(project);
	}
	
	@PostMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Project createProject(@Valid @RequestBody Project project) {
		Date creationDate = new Date();
		project.setCreateDate(creationDate);
		project.setUpdateDate(creationDate);
		return projectRepository.save(project);
	}

	@PutMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public ResponseEntity<Project> updateProject(@PathVariable("id") Long projectId,
			@Valid @RequestBody Project projectDetails) throws ResourceNotFoundException {
		Project project = projectRepository.findById(projectId)
				.orElseThrow(() -> new ResourceNotFoundException("Id " + projectId + " not found"));

		project.setName(projectDetails.getName());
		project.setStatus(projectDetails.getStatus());
		project.setDescription(projectDetails.getDescription());
		project.setUpdateDate(new Date());
		final Project updatedProject = projectRepository.save(project);
		return ResponseEntity.ok(updatedProject);
	}

	@DeleteMapping(path = "{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Map<String, Boolean> deleteProject(@PathVariable("id") Long projectId)
			throws ResourceNotFoundException {
		Project project = projectRepository.findById(projectId)
				.orElseThrow(() -> new ResourceNotFoundException("Id " + projectId + " not found"));

		projectRepository.delete(project);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
