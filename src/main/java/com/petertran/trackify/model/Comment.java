package com.petertran.trackify.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "comments")
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "content")
	@NotBlank(message = "Content is required")
	private String content;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "issue_id")
	@Min(1)
	private long issueId;
	
	@Column(name = "commenter_id")
	@Min(1)
	private long commenterId;
	
	public Comment() {
		
	}
	
	public Comment(String content, Date createDate, Date updateDate, long issueId, long commenterId) {
		this.content = content;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.commenterId = commenterId;
		this.issueId = issueId;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public long getIssueId() {
		return issueId;
	}
	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}

	public long getCommenterId() {
		return commenterId;
	}
	public void setCommenterId(long commenterId) {
		this.commenterId = commenterId;
	}
	
}
