package com.petertran.trackify.model;

public enum ERole {
	ROLE_USER, ROLE_ADMIN, ROLE_MODERATOR
}
