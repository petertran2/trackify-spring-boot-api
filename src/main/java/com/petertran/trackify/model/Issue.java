package com.petertran.trackify.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "issues")
public class Issue {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "priority")
	@NotBlank(message = "Priority is required")
	private String priority;
	
	@Column(name = "subject")
	@NotBlank(message = "Subject is required")
	private String subject;
	
	@Column(name = "status")
	@NotBlank(message = "Status is required")
	private String status;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "due_date")
	private Date dueDate;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@Column(name = "update_date")
	private Date updateDate;
	
	@Column(name = "project_id")
	@Min(1)
	private long projectId;
	
	@Column(name = "reporter_id")
	@Min(1)
	private long reporterId;
	
	@Column(name = "assignee_id")
	private long assigneeId;

	public Issue() {
		
	}
	
	public Issue(String priority, String subject, String status, String description, Date dueDate, Date createDate, Date updateDate, long projectId, long reporterId, long assigneeId) {
		this.priority = priority;
		this.subject = subject;
		this.status = status;
		this.description = description;
		this.dueDate = dueDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.projectId = projectId;
		this.reporterId = reporterId;
		this.assigneeId = assigneeId;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public long getReporterId() {
		return reporterId;
	}
	public void setReporterId(long reporterId) {
		this.reporterId = reporterId;
	}

	public long getAssigneeId() {
		return assigneeId;
	}
	public void setAssigneeId(long assigneeId) {
		this.assigneeId = assigneeId;
	}
	
}