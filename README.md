# Trackify API

![Trackify Home Page](https://gitlab.com/petertran2/trackify-react-front-end/-/raw/master/README_assets/trackify_react_demo.gif)

Trackify API is the Spring Boot RESTful API for [Trackify Issue Tracker](https://gitlab.com/petertran2/trackify-react-front-end), a React single-page web application that documents issues and software bugs to help people manage projects more effectively. This API allows client apps to list, show, create, update, and delete data from it.

Link: https://trackify-spring-boot.herokuapp.com/

For more information about the Trackify web app, please visit [its repository here](https://gitlab.com/petertran2/trackify-react-front-end).

## Back End Technologies

- Spring Boot
- Spring Framework
- Java 8
- JPA (Spring Data)
- PostgreSQL
- Maven
- JSON Web Tokens (JWT)
- Postman
- Eclipse
- Heroku

## Front End Technologies

- React
- Javascript (ES6+)
- jQuery
- Axios
- HTML
- CSS
- React Router
- Validator
- Moment.js
- NPM
- GitLab Pages (deployed with GitLab CI/CD)

### Author
Peter Tran
